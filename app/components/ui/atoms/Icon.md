An upload icon (you must specify a size).

```js
<Icon size={6}>Upload</Icon>
```

Upload failure icon.

```js
<Icon size={6}>UploadFailure</Icon>
```

Some icons accept additional parameters.

```js
<Icon size={6} percentage={100}>
  Upload
</Icon>
```
